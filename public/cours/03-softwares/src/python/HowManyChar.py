count = 0

# use input() if using python 3, raw_iput for python 2.7
my_string = raw_input("Entrer du texte, on va compter combien de r, il y a : ")
print(my_string)
my_char = "r"

for i in my_string:
    if i == my_char:
        count += 1

print(count)