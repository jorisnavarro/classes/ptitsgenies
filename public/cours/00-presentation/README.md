
# 00 Presentations

## Séquence de formation

Présentation

## Durée 

4h

## Ojectif

* Le contexte de la formation 
    * Connaitre le commanditaire (CD66)
    * connaitre le centre de formation (L'IDEM)
    * connaitre les consignes de sécurité/centre/covid ....
    * connaitre les horaires (pauses/dej, ...)

    * Programme de la formation
    * la méthodologie pédagogique
    * les évaluations
    * les criètres d'évaluation (50% PIX,  50% Présenation finale devant Jury)
    * Le projet final avec sa documentation

    * les règles droit à l’expérimentation et à l’erreur, de dire ce qu’on pense, de dire « stop » à un exercice que l’on a pas envie de faire

    * mise à l'aise
    * connaitre les objectifs de chacun

* Rapide découverte et prise en main du matériel
* Vérification du matériel: Ecrire son nom dans un tableur partagé


## contenu

* Présenation orale
* Prise en main du matériel
* Evaluation

## Matériel

## Effectué

Non

## Déroulé

Démarrer en position basse (facilitateur), salle en cercle

* Mise en confiance

* Ice breaker : 
    * Le cercle des prénoms :  Le groupe se place en cercle. Le premier doit dire son prénom et y associe un geste précis. Le deuxième reprend le prénom et le geste du premier et y ajoute les siens et ainsi de suite

* Energizer : 

* Check-in :

quizz
auto evaluation  -> [Grille](https://evolutionnumerique.com/carto/index_htm_files/dc_mv.pdf)


* Check-out:
attentes ?
Obstacles/peur ?

fiche individuelle swot
auto evaluation


passer en posture haute (salle en U) pour la thérorie

## Notes

Issus de l'[article sur comment bien démarrer la formation](https://www.cairn.info/revue-les-cahiers-internationaux-de-psychologie-sociale-2007-3-page-157.htm):
* la prise de contact constitue un des moments les plus importants (Karolewicz (1998), la pédagogie expérientielle)
* créer la confiance et mobiliser les participants
* créer un climat psychosociologique favorable
* en mettant les participants à l’aise et en les réconfortant, en facilitant la prise de risque
* La courtoisie et la chaleur des relations humaines, seule solution ?
* Ice breakers, mais attention à la lassitude et à l'anxiété (on donne en fonction de ce que l’on reçoit)
* Expression des représentations, des attentes ou des craintes: Metaplan, Philips 6x6, ou tours de table des attentes

[Démarrer avec la posture basse](https://nouvelletrace.fr/blog/formateur-vs-facilitateur/)

![http://4cristol.over-blog.com/2015/11/la-facilitation-position-basse-ou-position-haute.html](http://4cristol.over-blog.com/2015/11/la-facilitation-position-basse-ou-position-haute.html?utm_source=_ob_share&utm_medium=_ob_pinterest&utm_campaign=_ob_sharebar)

**pourquoi** et surtout **pour quoi** ?

Idées:
la météo
Prénom + animal + gest
shifumi (energizer)

Se connaitre : les prénoms
Cartographie des participants: Explorateurs, acheteurs, vacancier, prisonnier

Mettre à l'aise les participants

Connaitre l'ambiance du groupe
Souder le groupe: baton d'helium

créer un esprit d'équipe


#### [LE CERCLE DES PRÉNOMS ET DES GESTES](https://www.emydigital.fr/20-icebreaker-animation-ateliers/)

**Objectif** : Faire connaissance, développer l’écoute, l’observation, la concentration, la mémoire et la bienveillance d’un groupe.

**Principe** : Le groupe se place en cercle. Le premier doit dire son prénom et y associe un geste précis. Le deuxième reprend le prénom et le geste du premier et y ajoute les siens et ainsi de suite. Cet icebreaker développe l’écoute, l’observation, la mémorisation et la cohésion du groupe à travers l’attention que reçoit chacun, puisqu’on dit son prénom. Il s’inscrit dans le groupe en tant que personne. Si une personne a du mal à retenir, la bienveillance du groupe l’aide, l’intelligence collective peut ainsi s’installer tranquillement.

**Nombre de participants** : de 5 à 30

**Durée** : max 15 minutes

![](https://coworkinginlyon.fr/wp-content/uploads/2019/01/People-with-Question-Mark-Faces-1288x724.jpg)

#### [PRÉSENTATION CROISÉE SUPER-POUVOIR](https://www.emydigital.fr/20-icebreaker-animation-ateliers/)

**Objectif** : Cet icebreaker est conçu pour que les différentes personnes d’un groupe puissent apprendre à se connaître à travers un échange ludique.

**Principe** : Séparez le groupe de façon à avoir plusieurs binômes. Tour à tour, et par tranche de cinq minutes, une personne se chargera de poser des questions axées (une qualité, un métier, un talent…) afin de définir le super-pouvoir de son interlocuteur et de le dessiner en un temps record. L’autre se contentera d’y répondre avant d’échanger les rôles. Une fois le temps écoulé, chaque binôme présentera son partenaire au reste du groupe. (Par deux, les participant se posent des questions à tour de rôle pour dresser le profil de l’autre et découvrir son super pouvoir)

**Nombre de participants** : De 4 à 12

**Durée** : max 30 minute

![](https://www.metacartes.cc/wp-content/uploads/2018/11/presentation_croisee_NB_400.jpg)


#### [EXPLORATEUR, ACHETEUR, VACANCIER, PRISONNIER](https://www.emydigital.fr/20-icebreaker-animation-ateliers/)

**Objectif** : Le but est que chacun se positionne par rapport à un archétype.

Cet Ice-breaker permet de clarifier l’état d’esprit du groupe, les participants sont-ils heureux d’être présents ou au contraire se sentent-ils obligés ? L’animateur ainsi que les participants auront un avant-goût de l’énergie du groupe.

**Principe** : Expliquer le jeu aux participants : “Je vais vous demander d’indiquer, de manière anonyme, dans quelles conditions vous arrivez aujourd’hui, dans quel état d’esprit. Expliquez à quoi correspond l’explorateur, l’acheteur, le vacancier et le prisonnier, placer ces 4 archétypes sur un mur, un paperboard. Distribuer une gommette à chaque participant et demandez leur de venir positionner la gommette dans la case archétype qui leur correspond le plus.

**L’Explorateur** : Curieux de nature, l’explorateur souhaite découvrir, d’apprendre de nouvelles choses. Il vient pour toutes les informations possibles sur le projet, le produit… qui émergera de la réunion.

**L’Acheteur** : Les acheteurs sont intéressés par un sujet précis et souhaitent repartir avec des réponses.

**Le Vacancier** : Il n’est pas particulièrement intéressé par la réunion, mais c’est toujours mieux que de “travailler”.

**Le Prisonnier** : Il aurait préféré faire autre chose, mais n’a pas eu le choix.

Une fois que chacun s’est positionné, commentez le résultat et guidez une discussion sur ce que ça signifie pour le groupe.

**Nombre de participants** : de 6 à 40

**Durée** : 10 min

![](https://d3kohgq5akxhzp.cloudfront.net/wp-content/uploads/2020/01/15132051/icebreaker-explorateur-acheteur-vacancier-prisonnier.jpg)

#### [LE BATON D’HÉLIUM](https://www.emydigital.fr/20-icebreaker-animation-ateliers/)
Objectif : Pour souder le groupe et s’amuser.

Principe : Tous les participants portent, sur le bout des index, une longue tige légère (elle peut être en plastique, en papier ou tout autre matériel de récupération). Lancez le challenge de la descendre tous ensemble et de la poser au sol de manière horizontale. Cela vous semble simple ? C’est que vous n’avez pas essayé ! Comme on le voit sur cette vidéo, spontanément, la tige va monter, monter, monter… D’où son nom “bâton d’hélium”. Les participants vont donc apprendre à se faire confiance et se coordonner pour relever le challenge.

Nombre de participants : 20 personnes par bâton

Durée : 5 minutes

![le baton d'helium](https://cooperative-oasis.org/wiki/cache/image_lebatondhelium_baton_helium.jpg)

[My Agile Partner](https://blog.myagilepartner.fr/index.php/tag/serious-game/) - Autre blog contenant des serious games par catégories

 * [Ice breakers](https://blog.myagilepartner.fr/index.php/ice-breaker/)
    * 1  : Trouvez vos jumeaux [IB] [E]
    * 2  : Ice breaker daltonien [IB] [E]
    * 3  : Le Shifumi géant [IB] [E]
    * 4  : 2 vérités, 1 mensonge [IB]
    * 5  : dessin de visage collaboratif [IB]
    * 6  : dos à dos [IB] [E]
    * 7  : Marshmallow Challenge [IB][E]
    * 8  : Draw my sentence [IB][E]
    * 9  : Lost, les disparus [IB]
    * 10 : la pelote de laine [IB]
    * 11 : Les points communs [IB]
    * 12 : Le Face Wall [IB]
    * 13 : La cocotte agile [IB] [E]
    * 14 : Les smarties [IB]
    * 15 : Le nombre secret [IB] [E]
    * 16 : Le Mot Unique [IB]
    * 17 : Le Bâton d’Hélium [IB] [E]
    * 18 : La couverture [IB]
    * 19 : Qui fait quoi ? [IB]
    * 20 : L’après confinement [IB]
    * 21 : Le Speed Dating [IB]
    * 22 : Le mot juste [IB]
    * 23 : Les 20 questions [IB]
    * 24 : Le Ni Hao [IB] [E]
    * 25 : Le tour de table [IB]
    * 26 : Le tour de table inversé [IB]
    * 27 : Classement alphanumérique [IB] [E]
    * 28 : L’interview [IB]
    * 29 : Happy Salmon [IB] [E]
    * 30 : La chasse au trésor [IB] [E]
    * 31 : L’acronyme [IB]
    * 32 : Qui l’a fait ? [IB]

* [Energizers](https://blog.myagilepartner.fr/index.php/energizer-agile/)
    * 1 : le walk and stop ! [E]
    * 2 : en avant [E]
    * 3 : Les chaises non musicales [E]
    * 4 : make it rain [E]
    * 5 : le grand désordre [E]
    * 6 : la machine humaine [E]
    * 7 : Marshmallow Challenge [IB][E]
    * 8 : Draw my sentence [IB][E]
    * 9 : Le labyrinthe agile [E]
    * 10 : La cocotte agile [IB] [E]
    * 11 : Le nombre secret [IB] [E]
    * 12 : Le Bâton d’Hélium [IB] [E]
    * 13 : Le Ni Hao [IB] [E]
    * 14 : Classement alphanumérique [IB] [E]
    * 15 : Happy Salmon [IB] [E]
    * 16 : Trouvez vos jumeaux [IB] [E]
    * 17 : Ice breaker daltonien [IB] [E]
    * 18 : Le Shifumi géant [IB] [E]
    * 19 : dos à dos [IB] [E]
    * 20 : La chasse au trésor [IB] [E]

* [Check-in / Checkout](https://blog.myagilepartner.fr/index.php/check-out-check-in-agile/)
    * 1 : Le ROTI  [CO]
    * 2 : L’Happiness Door [CO]
    * 3 : Le mot de la fin [CO]
    * 4 : les 3 hashtags [CO]
    * 5 : l’échelle d’apprentissage [CI] [CO]
    * 6 : ma petite note personnelle [CO]
    * 7 : ressenti et apprentissage [CO]
    * 8 : le Meeting Spicer [CI][CO]

* [Team Building](https://blog.myagilepartner.fr/index.php/team-building-agile/)
    * 1 : Candy Love
    * 2 : Peer Introduction Game
    * 3 : Dans mes chaussures
    * 4 : Les rôles que nous jouons
    * 5 : Remember the Future
    * 6 : Le théâtre d’improvisation

* [Retrospective](https://blog.myagilepartner.fr/index.php/retrospective/)
    * 1 Le speed boat
    * 2 Hot-air balloon (montgolfière)
    * 3 Gladiator
    * 4 Starfish retrospective
    * 5 Les 3 petits cochons
    * 6 Le jeopardy
    * 7 : la fleur de lotus
    * 8 : turn on the table
    * 9 : la météo de l’humeur
    * 10 : le Fast and Furious
    * 11 : le Carrie Game
    * 12 : la rétrospective Lego
    * 13 : le Loup Garou
    * 14 : la rétro Bisounours
    * 15 : ESVP, la rétro des rétros
    * 16 : Glad, Sad, Mad
    * 17 : Comme au cinéma
    * 18 : Boite du futur
    * 19 : On refait le match
    * 20 : Dixit
    * 21 : Mario Bros
    * 22 : Rétrospective 4L
    * 23 : Keep / Drop / Start
    * 24 : Ombres chinoises
    * 25 : Speed Car
    * 26 : KAML – keep, add, more, less
    * 27 : Le SWOT
    * 28 : DAKI – drop, add, keep, improv
    * 29 : Matrice d’apprentissage
    * 30 : Kaamelott


### [Ebook Ice breakers et Energizer de l'Ecole de la facilitation](https://www.ecole-facilitation.fr/wp-content/uploads/2020/08/ebook-energizer_ok-20.pdf)

### [Atelier-collaboratif.com ](https://atelier-collaboratif.com/index.php)
